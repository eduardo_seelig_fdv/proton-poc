import sys

from proton import Message
from proton.handlers import MessagingHandler
from proton.reactor import Container

# MQ_HOST = 'activemq:5672'  # Docker activemq connection
MQ_HOST = 'amqps://b-c93efc1c-5150-417a-b721-6796b00b47aa-1.mq.us-east-1.amazonaws.com:5671'  # AWS activemq connection


class SendHandler(MessagingHandler):
    def __init__(self, address, message_body):
        super(SendHandler, self).__init__()

        self.conn_url = MQ_HOST
        self.address = address
        self.message_body = message_body

    def on_start(self, event):
        conn = event.container.connect(
            self.conn_url, user='admin', password='MatiasRuggieri'
        )
        event.container.create_sender(conn, self.address)

    def on_link_opened(self, event):
        print("SEND: Opened sender for target address '{0}'".format
              (event.sender.target.address))

    def on_sendable(self, event):
        message = Message({'msg': self.message_body, 'data': False})
        event.sender.send(message)

        print("SEND: Sent message '{0}'".format(message.body))

        event.sender.close()
        event.connection.close()

    def on_accepted(self, event):
        print("Delivery", event.delivery, "is accepted")

    def on_rejected(self, event):
        print("Delivery", event.delivery, "is rejected")


def main():
    try:
        address, message_body = sys.argv[1:3]
    except ValueError:
        sys.exit("Usage: send.py <address> <message-body>")

    handler = SendHandler(address, message_body)
    container = Container(handler)
    container.run()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
