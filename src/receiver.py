import sys

from proton.handlers import MessagingHandler
from proton.reactor import Container

# MQ_HOST = 'activemq:5672'  # Docker activeMq connection
MQ_HOST = 'amqps://b-c93efc1c-5150-417a-b721-6796b00b47aa-1.mq.us-east-1.amazonaws.com:5671'  # AWS activemq connection


class ReceiveHandler(MessagingHandler):
    def __init__(self, address, desired):
        super(ReceiveHandler, self).__init__()

        self.conn_url = MQ_HOST
        self.address = address
        self.desired = desired
        self.received = 0

    def on_start(self, event):
        conn = event.container.connect(
            self.conn_url,
            user='admin',
            password='MatiasRuggieri'
        )
        event.container.create_receiver(conn, self.address)

    def on_link_opened(self, event):
        print("RECEIVE: Created receiver for source address '{0}'".format
              (self.address))

    def on_message(self, event):
        message = event.message

        print("RECEIVE: Received message '{0}'".format(message.body))

        self.received += 1

        if self.received == self.desired:
            event.receiver.close()
            event.connection.close()


def main():
    try:
        address = sys.argv[1]
    except (IndexError, ValueError):
        sys.exit("Usage: receive.py <address> [<message-count>]")

    try:
        desired = int(sys.argv[2])
    except (IndexError, ValueError):
        desired = 0

    handler = ReceiveHandler(address, desired)
    container = Container(handler)
    container.run()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
