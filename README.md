# Activemq / proton poc

 - Se creo un docker con un activemq para poder hacer pruebas locales.
 - Se conecta al amazonMq de prueba de nubi (se necesita estar dentro de la vpn)
 - Se pueden enviar objetos en los mensajes sin problemas

En send.py y receiver.py hay una constante MQ_HOST que esta comentada, para poder elegir si conectar al local o al amazonMq.
## Para 'instalar'
```
docker network create amqpnet

docker-compose build
```
## Para jugar y probar

### Dejar corriendo un receiver
`bash receiver.sh`

### Enviar un mensaje
`bash sender.sh`

### Entrar al bash para correr scripts a mano
`docker-compose run proton-poc bash`